#!/bin/bash

# sauvegarde_borg.sh

Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
logfile='/root/bin/logs_horaires'
DESTDIR='/home/jpantinoux/repo'
SOURCEDIR='/home/jpantinoux/mint'
PROG='/usr/bin/borg'

# Sauvegarde
echo -e "----\n1 Lancement sauvegarde à : $Heure le $Date" >> $logfile
$PROG create $DESTDIR::{now} $SOURCEDIR
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "2 Sauvegarde terminée à  : $Heure le $Date\n" >> $logfile
# echo -e "--------------------------------------------" >> $logfile

# Rotation
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "----\n3 Lancemnt rotation à : $Heure le $Date" >> $logfile
$PROG prune -v --list --stats --keep-daily=7 --keep-weekly=4 --keep-monthly=6 $DESTDIR
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "4 Rotation terminée à : $Heure le $Date\n" >> $logfile
echo -e "++++++++++++++++++++++++++++++++++++++++++" >> $logfile

