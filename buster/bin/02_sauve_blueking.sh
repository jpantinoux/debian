#!/bin/bash

# 02_sauve_blueking.sh

Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
logfile='/root/bin/logs_horaires'
# DESTDIR='/home/archeo/repo'
DESTDIR2='jpantinoux@192.168.0.30:/home/jpantinoux/borg'
SOURCEDIR='/home/archeo/mint'
PROG='/usr/bin/borg'

# Sauvegarde serveur1
echo -e "++++++++++++++++  DEBUT $Date  +++++++++++++++++" >> $logfile
# echo -e "----\n1 Lancement sauvegarde à : $Heure le $Date" >> $logfile
# $PROG create $DESTDIR::{now} $SOURCEDIR >> $logfile 2>&1
# Date=$(date +%d-%m-%Y) 
# Heure=$(date +%T)
# echo -e "2 Sauvegarde terminée à  : $Heure le $Date\n" >> $logfile
# echo -e "--------------------------------------------" >> $logfile

# Rotation serveur1
# Date=$(date +%d-%m-%Y)
# Heure=$(date +%T)
# echo -e "----\n3 Lancement rotation à : $Heure le $Date" >> $logfile
# $PROG prune -v --list --stats --keep-daily=7 --keep-weekly=4 --keep-monthly=6 $DESTDIR >> $logfile 2>&1
# Date=$(date +%d-%m-%Y)
# Heure=$(date +%T)
# echo -e "4 Rotation terminée à : $Heure le $Date\n" >> $logfile
# echo -e "++++++++++++++++++++++++++++++++++++++++++" >> $logfile
# echo -e "--------------------------------------------" >> $logfile
# echo -e "---- Sauvegarde vers serveur2 en cours -----" >> $logfile



# Sauvegarde vers serveur2
echo -e "----\n5 Lancement sauvegarde sur blueking à : $Heure le $Date" >> $logfile
$PROG create $DESTDIR2::{now} $SOURCEDIR >> $logfile 2>&1
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "6 Sauvegarde terminée à  : $Heure le $Date\n" >> $logfile
# echo -e "--------------------------------------------" >> $logfile

# Rotation sur serveur2
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "----\n7 Lancement rotation sur blueking à : $Heure le $Date" >> $logfile
$PROG prune -v --list --stats --keep-daily=7 --keep-weekly=4 --keep-monthly=6 $DESTDIR2 >> $logfile 2>&1
Date=$(date +%d-%m-%Y)
Heure=$(date +%T)
echo -e "8 Rotation terminée à : $Heure le $Date\n" >> $logfile
echo -e "++++++++++++++++++  FIN  +++++++++++++++++++" >> $logfile

